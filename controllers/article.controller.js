const articleServices = require("../services/articles.services");

exports.topArticle = (req, res, next) => {
  articleServices.topArticle((error, results) => {
    if (error) {
      return next(error);
    }
    return res.status(200).send({
      success: true,
      message: "Success",
      data: results,
    });
  });
};
