const bcrypt = require("bcrypt");
const userServices = require("../services/user.services");
const dotenv = require("dotenv");

dotenv.config();

exports.register = (req, res, next) => {
  const { password } = req.body;

  const salt = bcrypt.genSaltSync(10);

  req.body.password = bcrypt.hashSync(password, salt);

  userServices.register(req.body, (error, results) => {
    if (error) {
      return res.status(200).send({
        success: false,
        message: error.message,
      });
    }
    return res.status(200).send({
      success: true,
      message: "Register Success!",
      data: new Buffer.from(JSON.stringify({ results })).toString("base64"),
    });
  });
};

exports.login = (req, res, next) => {
  const { email, password } = req.body;

  userServices.login({ email, password }, (error, results) => {
    if (error) {
      return res.status(200).send({
        success: false,
        message: error.message,
      });
    }
    return res.status(200).send({
      success: true,
      message: "Login Succes!",
      data: new Buffer.from(JSON.stringify(results)).toString("base64"),
    });
  });
};
