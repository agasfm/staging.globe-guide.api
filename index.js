const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const errors = require("./middlewares/errors.js");

const auth = require("./middlewares/auth.js");
const { unless } = require("express-unless");

const app = express();

dotenv.config();

mongoose
  .connect(process.env.MONGO_URL)
  .then(() => {
    console.log("DATABASE CONNECTED SUCCESFULLY!");
  })
  .catch((err) => {
    console.log("FAILED CONNECT TO DATABASE!");
  });

auth.authenticateToken.unless = unless;
app.use(
  auth.authenticateToken.unless({
    path: [
      { url: "/api/v1/auth/register", methods: ["POST"] },
      { url: "/api/v1/auth/login", methods: ["POST"] },
    ],
  })
);

app.use(express.json());

// initial routes
app.use("/api/v1/auth", require("./routes/v1/users.routes"));
app.use("/api/v1/inquiry", require("./routes/v1/inquiry.routes"));

app.use(errors.errorHandler);

app.listen(process.env.PORT || 4000, () => {
  console.log("SERVER IS RUNNING!");
});
