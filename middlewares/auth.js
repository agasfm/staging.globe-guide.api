const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");

dotenv.config();

function authenticateToken(req, res, next) {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  if (token == null)
    return res.status(401).send({
      success: false,
      message: "token undefined",
    });

  jwt.verify(token, process.env.SALT, (err, user) => {
    console.log(err);
    if (err)
      return res.status(401).send({
        success: false,
        message: err.name.concat("(").concat(err.message).concat(")"),
      });
    req.user = user;
    next();
  });
}

function generateAccessToken(email) {
  return jwt.sign({ data: email }, process.env.SALT.toString(), {
    expiresIn: "3d",
  });
}

module.exports = {
  generateAccessToken,
  authenticateToken,
};
