const mongoose = require("mongoose");
const { schema } = mongoose;

const articleSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    contentUrl: {
      type: String,
      required: true,
    },
    images: {
      type: Array,
      required: true,
    },
    author: {
      type: String,
    },
    is_active: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

articleSchema.set("toJSON", {
  transform: (document, returnedObject) => {
    returnedObject.id = returnedObject._id.toString();
    delete returnedObject._id;
    delete returnedObject.__v;
    //do not reveal passwordHash
    delete returnedObject.password;
  },
});

// userSchema.plugin(uniqueValidator, { message: "{PATH} already in use." });

const Articles = mongoose.model("articles", articleSchema);
module.exports = Articles;
