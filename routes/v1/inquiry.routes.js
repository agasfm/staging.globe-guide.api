const articleController = require("../../controllers/article.controller");
const express = require("express");
const router = express.Router();

router.post("/articles", articleController.topArticle);

module.exports = router;
