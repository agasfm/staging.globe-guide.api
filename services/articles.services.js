const Articles = require("../models/article.model");

async function topArticle(callback) {
  const articles = await Articles.find().limit(5);

  if (articles != null) {
    return callback(null, articles);
  }
}

module.exports = { topArticle };
