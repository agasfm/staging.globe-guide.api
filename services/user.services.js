const User = require("../models/user.model");
const bcrypt = require("bcrypt");
const auth = require("../middlewares/auth.js");

async function register(params, callback) {
  if (params.username === undefined) {
    return callback(
      {
        success: false,
        message: "Username Required",
      },
      ""
    );
  }
  if (params.email === undefined) {
    return callback(
      {
        success: false,
        message: "Email Required",
      },
      ""
    );
  }

  if (params.password === undefined) {
    return callback(
      {
        success: false,
        message: "Password Required",
      },
      ""
    );
  }

  if (params.firebase_token === undefined) {
    return callback(
      {
        success: false,
        message: "Firebase token Required",
      },
      ""
    );
  }

  const token = auth.generateAccessToken(params.email);

  const user = new User(params);
  user
    .save()
    .then((response) => {
      return callback(null, { ...user.toJSON(), token });
    })
    .catch((error) => {
      return callback(error);
    });
}

async function login({ email, password }, callback) {
  const user = await User.findOne({ email });

  if (user != null) {
    if (bcrypt.compareSync(password, user.password)) {
      const token = auth.generateAccessToken(email);
      return callback(null, { ...user.toJSON(), token });
    } else {
      return callback({
        message: "Invalid Password!",
      });
    }
  } else {
    return callback({
      message: "email Isn't registered!",
    });
  }
}

module.exports = { register, login };
